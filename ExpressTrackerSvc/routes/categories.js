﻿var sql = require('mssql');

var express = require('express');
var router = express.Router();

router.get('/Incomes', function (req, res) {
    console.log('income categories');
    
    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('ItemType', sql.Int, 2)
            .execute('stpCategories_GetAll').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

router.get('/Expenses', function (req, res) {
    console.log('Expense Categories');

    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('ItemType', sql.Int, 1)
            .execute('stpCategories_GetAll').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

module.exports = router;
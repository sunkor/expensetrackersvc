﻿var sql = require('mssql');

var express = require('express');
var router = express.Router();

//get cash item by identifier
router.get('/:id', function (req, res) {
    console.log('get cash item...');

    var itemId = req.params.id;

    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('ItemID', sql.Int, itemId)
            .execute('stpCashItem_Populate').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

//update cash item by identifier
router.put('/:id', function (req, res) {
    console.log('updating cash item...');

    var itemId = req.params.id;
    var date = req.body.date;
    var categoryid = req.body.categoryid;
    var amount = req.body.amount;
    var description = req.body.description;
    
    console.log('item id: ' + itemId + ', date: ' + date + ', category: ' + categoryid + ', amount: ' + amount + ', notes: ' + description);

    updateCashItem(itemId, date, categoryid, amount, description);
});

//delete cash item by identifier
router.delete('/:id', function (req, res) {
    console.log('deleting cash item...');

    var itemId = req.params.id;

    console.log('item id: ' + itemId);
    
    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('ItemID', sql.Int, itemId)
            .execute('stpCashItem_Delete').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

//create expense cash item
router.post('/expenses', function (req, res) {
    console.log('adding expense...');

    //var itemId = req.param('ItemID');
    var date = req.body.date;
    var categoryid = req.body.categoryid;
    var amount = req.body.amount;
    var description = req.body.description;

    console.log('date: ' + date + ', category: ' + categoryid + ', amount: ' + amount + ', notes: ' + description);

    updateCashItem(-1, date, categoryid, amount, description);
});

//create income cash item
router.post('/incomes', function (req, res) {
    console.log('adding income..');

    //var itemId = req.param('ItemID');
    var date = req.param('date');
    var categoryid = req.param('categoryid');
    var amount = req.param('amount');
    var notes = req.param('notes');

    console.log('date: ' + date + ', category: ' + category + ', amount: ' + amount + ', notes: ' + notes);

    updateCashItem(-1, date, categoryid, amount, notes);

});

//update cash item
function updateCashItem(itemId, date, categoryid, amount, description) {
    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('ItemID', sql.Int, itemId)
            .input('CategoryID', sql.Int, categoryid)
            .input('Date', sql.Date, date)
            .input('Amount', sql.Float, amount)
            .input('Description', sql.VarChar(1024), description)
            .execute('stpCashItem_Update').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
}

//sync - get cash items to update locally based on last update date time
router.get("/sync/:lastdatetimemodified", function (req, res) {

    var lastDateTime = req.params.lastdatetimemodified
    console.log('Sync - Get Cash Items after - ' + lastDateTime);

    var dt = new Date(Date.parse(lastDateTime));
    sql.connect(global.connectionString).then(function () {
        new sql.Request()
            .input('LastDateTimeModified', sql.DateTime, dt)
            .execute('stpCashItems_GetAll').then(function (recordsets) {
                console.dir(recordsets);
                res.send(recordsets);
            }).catch(function (err) {
                console.log(err);
                res.send(err);
            });
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

//sync - update cash items to server
router.post("/sync", function (req, res) {

    //var body = req.body;
    console.log('sync cash items...total items to sync: ' + req.body.length);

    sql.connect(global.connectionString).then(function () {
        for (var index = 0; index < req.body.length; index++) {
            var cashitem = req.body[index];
            var itemid = cashitem.itemid;
            var date = cashitem.date;
            var categoryid = cashitem.categoryid;
            var amount = cashitem.amount;
            var description = cashitem.description;
            var deleted = cashitem.deleted;
            var lastupdate = new Date(Date.parse(cashitem.lastupdate));

            new sql.Request()
                .input('ItemID', sql.Int, itemid)
                .input('CategoryID', sql.Int, categoryid)
                .input('Date', sql.Date, date)
                .input('Amount', sql.Float, amount)
                .input('Description', sql.VarChar(1024), description)
                .input('Deleted', sql.Bit, deleted)
                .input('LastUpdate', sql.DateTime, lastupdate)
                .execute('stpCashItem_Sync').then(function (recordsets) {
                    console.dir(recordsets);
                    res.send(recordsets);
                }).catch(function (err) {
                    console.log(err);
                    res.send(err);
                });
        }
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    });
});

module.exports = router;
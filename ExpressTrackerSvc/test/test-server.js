﻿process.env.NODE_ENV = 'development';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
//var Blob = require('../models/blob');

chai.use(chaiHttp);

describe('Blobs', function () {
    it('should list Expense Categories', function (done) {
        chai.request(server)
            .get('/categories/getIncomeCategories')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                //res.body[0].should.have.property('Code');
                done();
            });
    });

    it('should list Income Categories', function (done) {
        chai.request(server)
            .get('/categories/getExpenseCategories')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                done();
            });
    });
});